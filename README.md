# INFO263 Group A Assignment
This is the assignment for INFO263 Group A at UC. 

## Basic Structure

The app consists of a completely separated frontend and backend.

### Backend
The back end is a simple PHP API with two actions: `search` and `get_receipt`. The API should be used by sending requests to `/api.php`. All requests should be `GET` requests, and the request type should be specified with the `action` parameter. Both actions return JSON data.


`search` takes one argument, `receipt_no` and returns all invoices which match the given number (beginning matches). Example output:

```JSON
"GET http://localhost/api.php?action=search&reciept_no=D"

[{"invoice_id":"D884721","description":"Wheel alignment and charge"}]
```

`get_receipt` gets all the info on an invoice given its exact invoice  number. Example output:

```JSON
"GET http://localhost/api.php?action=get_reciept&reciept_no=D884721"

[{
    "invoice_id": "D884721",
    "description": "Wheel alignment and charge",
    "quantity": 2,
    "unit_price": 42.451,
    "date": "2018-04-13",
    "due_date": "2018-04-14",
    "customer_id": 5,
    "owner_id": 5,
    "owner_fname": "Elon",
    "owner_lname": "Musk",
    "owner_phone": "0245158741",
    "owner_email": "elon@mars.space",
    "vehicle_id": "3FS8DC7AS49328328",
    "vehicle_chassis": "DD6C-7842102",
    "vehicle_model": "Roadster",
    "vehicle_make": "Tesla",
    "technician_name": "Cooper",
    "inspection_date_time": "2018-06-05 11:20:00",
    "branch_id": 5,
    "branch_name": "Linwood",
    "branch_phone": "03-3745035",
    "branch_email": "linwood@tyretown.net.nz",
    "branch_address": "175 Linwood Avenue, Linwood, Christchurch, 8624",
    "branch_gst_registration": "97154714",
    "branch_fax": "0275488541"
}]
```

### Frontend

The frontend is written with vanilla Javascript, HTML, and CSS using Bootstrap. It uses AJAX to fetch data from the API, and a custom templating library which works with data tags to correctly template the data. It is a single web page and the different "pages" you see are always present, but hidden when not in use. These include the welcome page, a loading screen which will show on slow connections, and the page with the invoice itself. 

### Special Features
* Loads the data asynchronousl with AJAX.
* Search feature with suggestions, set up to work with large databases.
* Proper localized times and dates using JS i18n.