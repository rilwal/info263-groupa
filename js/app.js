/* This app is the main JavaScript code for
    INFO263 Group A's assignment */

(function () {
    "use strict";

    // Commonly used DOM elements
    var searchbox  = document.getElementById("search"),
        searchform = document.getElementById("search-form"),
        invoice    = document.getElementById("invoice"),
        loading    = document.getElementById("loading"),
        hero       = document.getElementById("hero"),
        dom_alert  = document.getElementById("alert");

    var api_base = "api.php";

    // Utility function to handle basic data binding
    // data_tag should be the tag to map to JS objects
    // jsob should be a JavaScript object to fill data from
    function data_bind(data_tag, jsob) {
        var dom_nodes = document.querySelectorAll(`*[${data_tag}]`);

        for (var i = 0; i < dom_nodes.length; i++) {
            var node = dom_nodes[i];
            var idx = node.getAttribute(data_tag);
            node.innerText = jsob[idx];
        }
    }

    // This cleans up the typography in the tables:
    // substituting +/- for "±" and x.y' with x°y′ 
    function clean_table_values() {
        var dom_nodes = document.querySelectorAll("td");
        for (var i = 0; i < dom_nodes.length; i++) {

            var node      = dom_nodes[i],
                plusminus = /\+\/\-/,
                degrees   = /(\d+)\.(\d+)'/;

            node.innerText = node.innerText.replace(plusminus, "±");
            node.innerText = node.innerText.replace(degrees,   "$1°$2′");
        }
    }

    // Quick utility function to do an AJAX get request
    function get(url, callback) {
        var X = new XMLHttpRequest();
        X.addEventListener("load", callback);
        X.open("GET", url);
        X.responseType = "json";
        X.send();
    }

    // Generate the datalist suggestions for searching for invoices
    // In this particular situation it would probably be better to just
    // always have every option available, but if the number of invoices
    // was very high, this approach may be better.
    function populate_options(arr) {
        var oplist = document.getElementById("search-suggestions");
        oplist.innerHTML = "";
        for (var i in arr) {
            var op = arr[i];
            var op_tag = document.createElement("option");
            op_tag.value = op["invoice_id"];
            oplist.appendChild(op_tag);
        }
    }

    // Query the database for invoices matching a query,
    // and return them as a JSON list of objects
    function get_search_suggestions(query) {
        get(`/${api_base}?action=search&receipt_no=${query}`, function (x) {
            populate_options(x.target.response);
        });
    }

    // Get the data for a receipt, and populate the form
    function get_receipt_data(number) {
        // First, hide the invoice since it's loading
        // and show the loading screen
        invoice.classList.add("hidden");
        hero.classList.add("hidden");
        loading.classList.remove("hidden");
        dom_alert.classList.add("hidden");

        get(`/${api_base}?action=get_receipt&receipt_no=${number}`, function (y) {
            var res = y.target.response;
            if (res.length > 0) {
                var a = res[0];

                // Set the window hash, allowing bookmarking
                // or direct linking to a particular invoice
                window.location.hash = a["invoice_id"];
                
                var inspection_dt = new Date(a["inspection_date_time"]);
                var due_dt = new Date(a["due_date"]);

                // Format dates correctly for NZ English
                a["inspection_date"] = inspection_dt.toLocaleDateString("en-NZ");
                a["inspection_time"] = inspection_dt.toLocaleTimeString("en-NZ");
                a["invoice_due_date"] = due_dt.toLocaleDateString("en-NZ");

                // Compute total Price
                a["total"] = a["unit_price"] * a["quantity"];

                // Here we have all data, now bind it
                data_bind("data-bind-receipt", a);
                clean_table_values();

                // Now the invoice is ready to view
                invoice.classList.remove("hidden");
                loading.classList.add("hidden");
                hero.classList.add("hidden");
            } else {
                if (number != "") {
                    dom_alert.innerText = `Unrecognized receipt number: ${number}`
                    dom_alert.classList.remove("hidden");
                }
                loading.classList.add("hidden");
                hero.classList.remove("hidden");
            }

        });
    }

    searchbox.addEventListener("change", function (x) {
        get_search_suggestions(x.target.value);
    });

    // Populate initial search suggestions
    get_search_suggestions("");

    searchbox.addEventListener("keydown", function (x) {
        if (x.key == "Escape") {
            x.target.value = "";
            get_search_suggestions("");
        }
    });

    searchform.addEventListener("submit", function (x) {
        get_receipt_data(searchbox.value);
        searchbox.value = "";
        get_search_suggestions("");
        return false;
    });

    window.addEventListener("hashchange", function() {
        get_receipt_data(window.location.hash.substr(1));
    });

    // If the hash is set, load the receipt from the hash number
    if (window.location.hash) {
        get_receipt_data(window.location.hash.substr(1));
    }

}());