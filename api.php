<?php
include 'config.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Two API methods
if ($_GET["action"] == "get_receipt") {

    $receipt_no = $_GET["receipt_no"];

    $sql = "SELECT * FROM invoice "
         . "JOIN owner ON invoice.customer_id = owner.owner_id "
         . "JOIN vehicle ON owner.owner_id = vehicle.owner_id "
         . "LEFT JOIN inspection ON inspection.vehicle_id = vehicle.vehicle_id "
         . "LEFT JOIN branch ON branch.branch_id = inspection.branch_id "
         . "WHERE invoice.invoice_id = ?";
         
    $stmnt = $conn->prepare($sql);

    if ($stmnt == FALSE) {
        die('prepare() failed: ' . htmlspecialchars($conn->error));
    }

    $stmnt->bind_param("s", $receipt_no);

}
elseif ($_GET["action"] == "search") {
    $search = $_GET["receipt_no"] . '%';

    $search_query = "SELECT invoice_id, description FROM invoice "
                  . "WHERE invoice_id LIKE ? LIMIT 10";

    $stmnt = $conn->prepare($search_query); //compiles search_query
    $stmnt->bind_param("s", $search); //substitutes ? for receipt_no

}


$stmnt->execute();
$rows = $stmnt->get_result();
$rows = json_encode($rows->fetch_all(MYSQLI_ASSOC));

$conn->close();

print $rows;
?>